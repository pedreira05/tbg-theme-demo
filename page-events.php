<?php
/**
 * Template Name: Events
 *
 * 
 */

get_header();



?>


<?php get_template_part( 'components/block', 'banner' ) ?>



<?php if($_GET && $_GET['event_id']): ?>
<section class="announcement py-5" id="selected_event" name="selected_event">
	<?php 
		$post_id = $_GET['event_id'];
		$query_args = array(
			'post_type' => 'cpt_event', 
			'p' => $post_id
		);
		$loop = new WP_Query( $query_args );
		if($loop->have_posts()):
			while ( $loop->have_posts() ) : $loop->the_post(); 
				$heading = get_the_title();
				$content = get_the_content();
				$content = apply_filters('the_content', $content);
				$content = str_replace(']]>', ']]&gt;', $content);
				$image = get_field('image');
				$date_time = get_field('date');

				$content_array = array(
					'heading'		=>		$heading,
					'content'		=>		$content,
					'image'			=>		$image['sizes']['two_column_block'],
					'date'			=>		$date_time,
					'section'		=>		'events'
				);
				
			endwhile;
		endif;

		
	 ?>
	<?php get_template_part( 'components/block', 'image-right' ) ?>
	<?php wp_reset_query(); ?>
</section>
<?php endif; ?>

<section class="py-5 bg-dark-shade">
	<div class="container">
		<h1 class="text-center mb-3" style="color:#fff">Upcoming Events</h1>
		<div class="row">
			
	
			<?php
				// find date time now
				$date_now = date('Y-m-d H:i:s');
				$time_now = strtotime($date_now);

				$query_args = array(
					'post_type' => 'cpt_event', 
					'posts_per_page' => 9,
					'meta_query' 		=> array(
							array(
						        'key'			=> 'date',
						        'compare'		=> '>=',
						        'value'			=>  $date_now,
						        'type'			=> 'DATETIME'
						    )
					    ),
					'order'				=> 'ASC',
					'orderby'			=> 'meta_value',
					'meta_key'			=> 'date',
					'meta_type'			=> 'DATETIME'
					
				);
				$loop = new WP_Query( $query_args );
					
			?>
			<?php if($loop->have_posts()): ?>
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php 
						$image = get_field('image');
						$date_time = get_field('date');
						$end_date = get_field('end_date');
						$title = get_the_title( );
						$post_id = get_the_ID();
						$category_title = get_the_terms($post_id,'tbg_events' );
						
						
						
						$content_array = array(
							
							'image'			=>		$image['sizes']['medium'],
							'start_date'			=>		$date_time,
							'end_date'			=>		$end_date,
							'category_title'			=>		$category_title,
							'title'			=>		$title,
							'post_id'			=>		$post_id
						);
					 ?>

					 <div class="col-sm-4 mb-3">
					 	<a class="no-underline" href="?event_id=<?php echo $post_id; ?>#selected_event">
							<?php get_template_part( 'components/template', 'date-card' ) ?>
					 	</a>
					 </div>
				<?php endwhile; ?>
			<?php else: ?>
				<div class="col">
					<p class="text-center">
						<?php echo "No events at this time. Please check back soon!" ?>
					</p>
				</div>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
		</div>

		
		
	</div>
</section>
<section class="py-5">
	<div class="container">
		<div class="row">

			<?php

				$query_args = array(
					'post_type' => 'cpt_event', 
					'posts_per_page' => 3,
					'meta_query' 		=> array(
						array(
					        'key'			=> 'date',
					        'compare'		=> '<',
					        'value'			=>  $date_now,
					        'type'			=> 'DATETIME'
					    )
				    ),
				    'order'				=> 'ASC',
				    'orderby'			=> 'meta_value',
				    'meta_key'			=> 'date',
				    'meta_type'			=> 'DATETIME'
					
				);
				$loop = new WP_Query( $query_args );
					
			?>
			<?php if( $loop->have_posts() ): ?>
				<h2>Recent Events</h2>
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<div class="mb-3">
						<h5><?php the_title() ?></h5>
						<p><?php the_excerpt() ?></p>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
			
		</div>
		<div class="row">
			<div >
				<a href="<?php echo get_post_type_archive_link('cpt_event'); ?>" class="btn btn-primary">All Events</a>
			</div>
		</div>
	</div>
	
</section>


<?php 
	$video = get_field('video');
	$section_title = get_field('section_title');
 ?>


<section class="testimonies py-5 bg-dark-shade invert-text-color">
	<div class="container text-center">
		<?php if($section_title): ?>
			<h2><?php echo $section_title ?></h2>
		<?php endif; ?>
		<?php if($video): ?>
		<div class="video-img-container text-center">
			<?php echo $video; ?>
		</div>
		<?php endif; ?>
		<div class="text-center py-1">
			<a class="btn btn-primary" href="<?php echo tbg_get_category_link( 'testimonials' ) ?>">Testimonials</a>
		</div>
	</div>
</section>


<section class="book-appointment py-5" id="book-event" name="book-event">
	<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-6">
					<h2>Book Sylane for your Event</h2>
					<?php echo do_shortcode( '[tbg_contact_form]'); ?>
				</div>
			</div>
	</div>
</section>



<?php get_footer(); ?>
