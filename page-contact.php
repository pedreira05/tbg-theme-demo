<?php
/**
 * Template Name: Contact Us Page
 *
 * 
 */

get_header();



?>

<?php get_template_part( 'components/block', 'banner' ) ?>

<section class="bg-light-shade p-4">
	<div class="container ">
		<div class="row center-frame  mb-5 rounded">
			
				<div class="col align-self-center">
					<?php the_field('introduction'); ?>
				</div>
				<div class="col">
					<?php the_field('map'); ?>
				</div>
			
			
		</div>
	</div>
</section>

<section class=" p-4">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="card p-3 bg-primary-accent invert-text-color">
					<h2><?php the_field('form_title'); ?></h2>
					<?php echo do_shortcode( '[tbg_contact_form]' ) ?>
				</div>
			</div>
			<div class="col">
				<div class="p-3">
					<h2>Connect with Us</h2>
					<ul class="connect-with-us contact-us-page">
						<?php get_template_part( 'components/content', 'social-media' ) ?>
					</ul>
				</div>
			</div>
			
		</div>
	</div>
</section>

<?php get_footer(); ?>
