<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();
?>


<section class="mb-4">
	<div class="jumbotron jumbotron-fluid image-filter page-banner">
	  <div class="banner-content-wrap">
		  <div class="container">
		  	<div class="row align-items-center">
		  		<div class="col-6">
				    <?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
		
		  		</div>
		  	</div>
		  </div>
	  </div>
	</div>
</section>

<section >

	<div class="row">
		<div class="col-md-8">
			<?php if(is_post_type_archive('cpt_event')): ?>
				<div class="container">
					<div class="row">
			<?php endif; ?>

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php if(is_post_type_archive('cpt_event')): ?>
							<?php
								$post_id = get_the_ID();
								$image = get_field('image');
								$image = $image['sizes']['medium'];
								$date_time = get_field('date');
								$end_date = get_field('end_date');
								$start_date_month = date("M", strtotime($date_time));
								$start_day = date("d", strtotime($date_time));
								$title = get_the_title( );
							?>
							<div class="col-md-6 mb-4">
								
								<div class="card date-card box-shadow h-100 ">

									<div class="card-header" style="background: url(<?php echo $image ?>); background-size:cover">
										<div class="date">
											<span class="month"><?php echo $start_date_month ?></span> <br> 
											<span class="day"><?php echo $start_day; ?></span>
										</div>
										
									</div>
									<div class="card-body">
										
										<p class="category conferences">
										<?php $category_title = get_the_terms($post_id,'tbg_events' ); ?>
										<?php foreach ($category_title as $category):?>
											<?php echo $category->name . " "; ?>
										<?php endforeach; ?>
										<?php wp_reset_postdata() ?>
										</p>
										<h5 class="card-title"><?php echo $title ?></h5>
										<p class="mt-2">
											<a class="link" href="about-us/events?event_id=<?php the_ID(); ?>#selected_event">Details >></a>
										</p>
									</div>
									
								</div>
							</div><!-- col -->
						<?php elseif(is_category('testimonials')): ?>

							<?php  get_template_part( 'loop-templates/content', 'testimonials'); ?>

						<?php else: ?>
							
								<?php  get_template_part( 'loop-templates/content', 'tbg'); ?>
							
						
						<?php endif; ?>

					<?php endwhile; ?>

				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>

			

			<!-- The pagination component -->
			<div class="px-4 py-2">
				<div class="col">
					<?php understrap_pagination(); ?>
				</div>
			</div>
			<?php if(is_post_type_archive('cpt_event')): ?>
				</div><!-- row for event archive -->
			</div><!--container for event archive -->
			<?php endif; ?>
		</div><!-- col -->

		<div class="col">
			<div class="search-container" style="max-width:100%;">
				<?php get_search_form( $echo = true ) ?>
				
			</div>
		</div>
			
		

	</div> <!-- .row -->



</section><!-- Wrapper end -->

<?php get_footer(); ?>
