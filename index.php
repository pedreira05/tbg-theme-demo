<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
?>



	<section class="main-slider">
		<?php get_template_part( 'components/main', 'slider' ); ?>
	</section>

	<section class="appearances-container">
		<?php get_template_part( 'components/main', 'appearances' ) ?>
	</section>

	<section class="todays-devotional py-5">
		<?php get_template_part( 'components/main', 'devotional' ) ?>
	</section>

	<section class="book-appointment py-5 bg-dark-shade invert-text-color">
		<div class="container">
				<?php get_template_part( 'components/main', 'book-appointment' ); ?>
		</div>
	</section>
	

	<section class="upcoming-events py-5">
		<div class="container">
				<?php get_template_part( 'components/main', 'upcoming-events' ); ?>
		</div>
	</section>

	<section class="book-sylane py-5 bg-dark-shade invert-text-color">
		<div class="container">
				<?php get_template_part( 'components/main', 'book-sylane' ); ?>
		</div>
	</section>



	<section class="ping py-5">
		<?php get_template_part( 'components/main', 'ping' ) ?>
	</section>

	<?php $convinced_bg_image = get_stylesheet_directory_uri() . '/images/bg-convinced.jpg'; ?>
	<section class="order-convinced radial-overlay" style="background: url(<?php echo $convinced_bg_image ?>); background-size: cover">
		<?php get_template_part( 'components/main', 'order-convinced' ) ?>
	</section>

	<section class="latest-newsletter py-5 ">
		<?php get_template_part( 'components/main', 'newsletter' ) ?>
	</section>

	


<?php get_footer(); ?>
