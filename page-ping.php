<?php
/**
 * Template Name: PING Page
 *
 * 
 */

get_header();



?>

<?php get_template_part( 'components/block', 'banner' ) ?>



<section class="announcement py-5">
	<?php 
		$heading = get_field('intro_and_image_heading');
		$content = get_field('intro_and_image_content');
		$image = get_field('intro_and_image_image');
		$content_array = array(
			'heading'		=>		$heading,
			'content'		=>		$content,
			'image'			=>		$image['sizes']['two_column_block']
		);
	 ?>
	<?php get_template_part( 'components/block', 'image-right' ) ?>
</section>



<section class="py-5 bg-dark-accent invert-text-color">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-10">
				<p style="font-size:22px">


					<?php 
						$posts = get_field('testimony');

						if( $posts ): ?>
						   
						    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
						        <?php setup_postdata($post); ?>
						        <blockquote>
						        	
						            <?php the_content(); ?>
						            <span class="subdue-text" style="font-size: 1.8rem;"><?php the_field('authors_name'); ?></span>
						        </blockquote>
						        
						    <?php endforeach; ?>
						    
						    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
						<?php endif; ?>

								
					
				</p>
			</div>

		</div>
	</div>

</section>


<section class="py-5">
	<?php get_template_part( 'components/main', 'ping' ) ?>
</section>



<?php get_footer(); ?>
