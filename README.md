## Intro

This is a custom theme built using the Bootstrap-powered, [understrap theme](https://understrap.com/). It grants the owner the opportunity to edit content via [Advanced Custom Fields](https://advancedcustomfields.com/) so the advanced, localized acf-json folder is included.  All custom code on this theme was contributed by me.