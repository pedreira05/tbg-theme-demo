
<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>



<div class="wrapper py-0 bg-dark-shade invert-text-color" id="wrapper-footer">
	<div class="footer-top footer py-5 text-light bg-dark">
		<div class="container">
			<div class="row">
				<div class="col-lg col-md-6 tbg-summary">
					<h5 class="mb-0">Transformed By Grace, Inc. </h5>
					<p>Counseling and Conferencing Ministries</p>
					<p>Offering hope, healing and transformation</p>
				</div> 
				<div class="col-lg-2 col-md-3 pl-lg-3 col-6 py-4 py-md-0">
					<h5>About Us</h5>
					<?php wp_nav_menu( array( 'theme_location' => 'footer_left', 'menu_class' => 'about-us footer-nav' ) ); ?>
					
				</div>
				<div class="col-lg-2 col-md-3 col-6 py-4 py-md-0">
					<h5>Services</h5>
					<?php wp_nav_menu( array( 'theme_location' => 'footer_right', 'menu_class' => 'services footer-nav' ) ); ?>
				
				</div>
				<div class="col-lg col-md-12 pt-md-4 pt-lg-0">
					<h5>Connect with Us</h5>
					<ul class="connect-with-us footer-nav">
						<?php get_template_part( 'components/content', 'social-media' ) ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright-footer">
		<div class="container">
			<div class="row">
				<div class="col text-center py-1">
 					<p>
					<?php // update copyright each year


	                  $time = time(); 
	                  //This line gets the current time off the server
	                 
	                  $year= date("Y", $time); 
	                  //This line formats it to display just the year
	                  
	                  $startyear = "2011";
	                  //The starting year
	                  
	                  //if the startyear is the same as current yearjust print the start year  otherwise print from the start year to the current year
	                  if ($startyear==$year)
	                  $copy = "Copyright &copy; " . $startyear;
	                  else
	                  $copy = "&copy; Copyright " . $startyear . " - " . $year;
	                 
	                  echo $copy;
	                  //this line prints out the copyright date range
	                  ?>
	                  Transformed by Grace, Inc. All Right's Reserved
	              	</p>
				</div>
			</div>
		</div>
	</div>

	

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

