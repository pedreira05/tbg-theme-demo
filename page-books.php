<?php
/**
 * Template Name: Order Books
 */

get_header();


?>


	<?php get_template_part( 'components/block', 'banner' ) ?>

	<?php if( have_rows('books') ):

	 	// loop through the rows of data
	    while ( have_rows('books') ) : the_row();
	?>
   <section class="py-5">
   	<div class="container">
   		<div class="row">
   			<div class="col-sm-4">
   				<?php 
   					$the_image = get_sub_field('image');
   					$image = $the_image['url'];

   				 ?>
   				 <img src="<?php echo $image ?>">
   			</div>
   			<div class="col align-self-center">
               <div class="p-3">
                  <h2><?php the_sub_field('title') ?></h2>
                  <?php the_sub_field('description') ?>
                  <p>
                     <?php $purchase_info = get_sub_field('link_to_purchase') ?>
                     <a href="<?php the_sub_field('url') ?>" class="btn btn-<?php the_sub_field('style') ?>" target="_blank"><?php the_sub_field('text') ?></a>
                  </p>
               </div>
   			</div>
   		</div>
   	</div>
   	
   </section>

	 <?php   endwhile; ?>

	<?php endif; ?>

	

	


<?php get_footer(); ?>
