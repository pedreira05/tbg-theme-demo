<?php
/**
 * Template Name: Counseling Ministry Page
 *
 * 
 */

get_header();



?>

<?php get_template_part( 'components/block', 'banner' ) ?>


<section class="announcement py-5">
	<?php 
			$heading = get_field('intro_and_image_heading');
			$content = get_field('intro_and_image_content');
			$image = get_field('intro_and_image_image');
			$content_array = array(
				'heading'		=>		$heading,
				'content'		=>		$content,
				'image'			=>		$image['sizes']['two_column_block']
			);

		 ?>
		<?php get_template_part( 'components/block', 'image-right' ) ?>
</section>




<section class="book-appointment py-5 bg-dark-shade invert-text-color">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-6 text-center">
				<h2>Book an appointment</h2>
				<button class="btn btn-primary">Book an appointment</button>
			</div>
		</div>
	</div>
</section>

<section class="py-5">
	<div class="container">
		<h1 class="text-center mb-3">Our Counselors</h1>
		<div class="row">
			<?php 
			$args = array( 
				'post_type' => 'tbg_staff',
			 	'posts_per_page' => 30,
			 	'meta_key' => 'last_name',
		 	    'orderby' => 'meta_value',
		 	    'order' => 'ASC'
			 );
			$loop = new WP_Query( $args );

			$counselors = $loop->posts;
			function cgpSearchArray($counselors, $field="post_name", $value="sylane-mack")
			{
			   foreach($counselors as $key => $counselor)
			   {
			      if ( $counselor->$field === $value )
			         return $key;
			   }
			   return false;
			}

			$sylane_key = cgpSearchArray($counselors);

			$sylane = $counselors[$sylane_key];

			unset($counselors[$sylane_key]);

			array_unshift($counselors, $sylane);


			$loop->posts = $counselors;

					
			while ( $loop->have_posts() ) : $loop->the_post();
				$staff = array();
				$staff['first_name'] = get_field('first_name');
				$staff['last_name'] = get_field('last_name');
				$staff['biography'] = get_field('biography');
				$image = get_field('image');
				$staff['image'] = $image['sizes']['medium'];
				$staff['image_alt'] = $image['alt'];
				$staff['titles'] = get_field('titles');
				$staff['ID'] = get_the_ID();
			?>
			<?php get_template_part( 'components/block', 'counselors' ) ?>
			<?php endwhile; ?>
			<?php wp_reset_query() ?>
		</div>
	</div>
</section>

<section class="book-appointment py-5 bg-dark-accent invert-text-color" id="book-your-appointment" name="book-your-appointment">
	<div class="container">
			<div class="row justify-content-center">
				
			
				<div class="col-sm-8">
					<h2>Book an appointment</h2>
					<?php echo do_shortcode( '[tbg_contact_form]' ) ?>
					</form>
				</div>
			</div>
	</div>
</section>

<?php get_footer(); ?>
