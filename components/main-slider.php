 <?php if( have_rows('carousel') ): ?>
	<?php 
		$carousel = get_field('carousel');
		$slide_total = count($carousel); 
		$slide_count = 0;

		
	?>
 	<div id="tbg_carosel" class="carousel slide bg-light-shade" data-ride="carousel" data-pause="false">
 		     
 		<div class="carousel-inner">
	 		<?php while ( have_rows('carousel') ) : the_row(); ?>
		 		<?php 
		 			$image_arr = get_sub_field('image');
		 			$image = $image_arr['sizes']['main_slider'];
		 			$active_class = ($slide_count == 0) ? 'active' : '';
					$subtitle_on_top = get_sub_field('show_subtitle_on_top');
					$slide_color = get_sub_field('slide_color');

					switch ($slide_color) {
						case 'green':
							$slide_color = "bg-secondary-accent";
							break;
						case 'brown':
							$slide_color = "bg-dark-accent";
							break;
						case 'pink':
							$slide_color = "bg-light-accent";
							break;
						case 'red':
							$slide_color = "bg-primary-accent";
							break;
						
						default:
							$slide_color = "bg-secondary-accent";
							break;
					}
		 		?>
	 		    <div class="carousel-item <?php echo $active_class ?>">
	 		    	<div class="row no-gutters <?php echo $slide_color; ?> invert-text-color">
	 		    		<div class="col-lg-4 slide-caption align-self-center">
	 			      		<div class="p-3">
	 			      			<?php if(!empty($subtitle_on_top) && 'true' == $subtitle_on_top[0]): ?>
		 				      		<p><?php the_sub_field('subtitle'); ?></p>
		 				      		<h1><?php the_sub_field('title'); ?></h1>
	 				      		<?php else: ?>
		 				      		<h1><?php the_sub_field('title'); ?></h1>
		 				      		<p><?php the_sub_field('subtitle'); ?></p>
	 				      		<?php endif; ?>
	 			      		</div>
	 		    		</div>
	 		    		<div class="col-lg-8 slide-image">
	 		    			<div class="slide-image-frame" style="background: url(<?php echo $image; ?>) no-repeat center center; background-size:cover"></div>
	 		    		</div>
	 		    	</div>
	 		    </div>

	 		    <?php $slide_count++; ?>

 			<?php endwhile; ?>
 			<?php wp_reset_query(); ?>
 		</div>
 		    
 		    
		  <div class="container">
		 
			  <div class="row carousel-controls px-4 no-gutters">
			  	<div class="col-sm-4">
			  		
					  <ol class="carousel-indicators">
					  	<?php for ($i=0; $i < $slide_total; $i++) : ?>
					  	<?php $active_class = ($i == 0) ? 'active' : ''; ?>
					     <li data-target="#tbg_carosel" data-slide-to="<?php echo $i ?>"  class="<?php echo $active_class; ?>"></li>
					  	<?php endfor; ?>
					     
					   </ol>
			     		<div class="next-prev">
			   	  		<a class="carousel-control-prev" href="#tbg_carosel" role="button" data-slide="prev">
			   	  		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			   	  		    <span class="sr-only">Previous</span>
			   	  		  </a>
			   	  		  <a class="carousel-control-next" href="#tbg_carosel" role="button" data-slide="next">
			   	  		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			   	  		    <span class="sr-only">Next</span>
			   	  		  </a>
			     		</div>
			  	</div>
			  
			  </div>
		  </div>
 		  
 	</div>

    
 <?php endif; ?>
