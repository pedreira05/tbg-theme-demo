
<div class="container" >
	<div class="row">
		<div class="col align-self-center py-3 py-md-0">
			<h2>Order Convinced!</h2>
			<p>God’s transforming love story written in Sylane Mack’s life.</p>
			<a href="/order-books" class="btn btn-primary">Order now</a>
		</div>
		<div class="col">
			<div class="text-center">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/convinced_book_cover.jpg" class="box-shadow rounded" style="max-width: 330px;">
			</div>
		</div>
	</div>
</div>