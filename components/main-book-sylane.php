<div class="row">
	<div class="col text-center">
		<h1>Book Sylane for your next event</h1>
		<p>Powerful, transformative messages, grounded in God's word, and shared with wisdom, humor and hope</p>
		<a href="/about-us/speaking-topics"  class="btn btn-secondary">Learn More</a>
		<a href="/about-us/speaking-topics#book-appt" type="submit" class="btn btn-primary">Book Now</a>
	</div>
</div>