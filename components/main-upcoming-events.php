<?php 
// find date time now
$date_now = date('Y-m-d H:i:s');
$time_now = strtotime($date_now);
 ?>

<div class="row">
	<div class="col-lg-4 align-self-center pb-4 pb-lg-0">
		<div class="event-intro">
			<h1>Our Events</h1>
			<p>Transformed by Grace provides conference speaking for conferences and retreats, and hosts community seminars at the Counseling and Wellness center.</p>
			<p><a href="about-us/events"  class="btn btn-primary">View All Events</a></p>
		</div>
	</div>
	
		<?php    
				  
				   
				  $query_args = array(
				  	'post_type' => 'cpt_event', 
				  	'posts_per_page' => 1,
				  	'tax_query' => array(
				  			array(
				  				'taxonomy' => 'tbg_events',
				  				'field'    => 'slug',
				  				'terms'    => 'conferences-and-retreats',
				  			),
				  		),
				  	'meta_query' 		=> array(
				  		'relation'		=>		'AND',
			  			array(
			  		        'key'			=> 'end_date',
			  		        'compare'		=> '>=',
			  		        'value'			=>  $date_now
			  		    )

			  	    )
				  );
				  $loop = new WP_Query( $query_args );

				  	
				?>
				<?php if($loop->have_posts()): ?>
					<div class="col-lg-4 mb-2 mb-lg-0">
					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
						<?php 
							$image = get_field('image');
							$date_time = get_field('date');
							$end_date = get_field('end_date');
							$title = get_the_title( );
							$post_id = get_the_ID();
							$category_title = get_the_terms($post_id,'tbg_events' );
							

							$content_array = array(
								'image'			=>		$image['sizes']['medium'],
								'start_date'			=>		$date_time,
								'end_date'			=>		$end_date,
								'category_title'			=>		$category_title,
								'title'			=>		$title,
								'post_id'			=>		$post_id
							);
							$image = $content_array['image'];
							$start_date = $content_array['start_date'];
							$start_date_month = date("M", strtotime($start_date));
							$start_day = date("d", strtotime($start_date));
							$end_date = $content_array['end_date'];
							$end_date_month = date("M", strtotime($end_date));
							$category_title = $content_array['category_title'];

							$title = $content_array['title'];
							$post_id = $content_array['post_id'];
						 ?>

					
						 	<a class="no-underline" href="about-us/events?event_id=<?php echo $post_id; ?>#selected_event">
								<div class="card date-card box-shadow h-100">

									<div class="card-header" style="background: url(<?php echo $image ?>) center top; background-size:cover";>
										<div class="date">
											<span class="month"><?php echo $start_date_month ?></span> <br> 
											<span class="day"><?php echo $start_day; ?></span>
										</div>
										
									</div>
									<div class="card-body">
										
										<p class="category conferences">
										<?php foreach ($category_title as $category):?>
											<?php echo $category->name . " "; ?>
										<?php endforeach; ?>
										</p>
										<h5 class="card-title"><?php echo $title ?></h5>
										
									</div>
									
								</div>
						 	</a>
						 
					<?php endwhile; ?>
					</div>
				<?php else: ?>
					
				<?php endif; ?>
				<?php wp_reset_query(); ?>
	
		<?php    
		  
		   
		  $query_args = array(
		  	'post_type' => 'cpt_event', 
		  	'posts_per_page' => 1,
		  	'tax_query' => array(
		  			array(
		  				'taxonomy' => 'tbg_events',
		  				'field'    => 'slug',
		  				'terms'    => 'community-seminars',
		  			),
	  		),
  		  	'meta_query' 		=> array(
  		  		'relation'		=>		'AND',
  	  			array(
  	  		        'key'			=> 'end_date',
  	  		        'compare'		=> '>=',
  	  		        'value'			=>  $date_now
  	  		    )

  	  	    )
		  	
		  );
		  $loop = new WP_Query( $query_args );

		?>
		<?php if($loop->have_posts()): ?>
			<div class="col mb-2 mb-sm-0">
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<?php 
					$image = get_field('image');
					$date_time = get_field('date');
					$end_date = get_field('end_date');
					$title = get_the_title( );
					$post_id = get_the_ID();
					$category_title = get_the_terms($post_id,'tbg_events' );
					

					$content_array = array(
						'image'			=>		$image['sizes']['card_head'],
						'start_date'			=>		$date_time,
						'end_date'			=>		$end_date,
						'category_title'			=>		$category_title,
						'title'			=>		$title,
						'post_id'			=>		$post_id
					);
					$image = $content_array['image'];
					$start_date = $content_array['start_date'];
					$start_date_month = date("M", strtotime($start_date));
					$start_day = date("d", strtotime($start_date));
					$end_date = $content_array['end_date'];
					$end_date_month = date("M", strtotime($end_date));
					$category_title = $content_array['category_title'];

					$title = $content_array['title'];
					$post_id = $content_array['post_id'];
				 ?>

			
				 	<a class="no-underline" href="about-us/events?event_id=<?php echo $post_id; ?>#selected_event">
						<div class="card date-card box-shadow h-100">

							<div class="card-header" style="background: url(<?php echo $image ?>); background-size:cover">
								<div class="date">
									<span class="month"><?php echo $start_date_month ?></span> <br> 
									<span class="day"><?php echo $start_day; ?></span>
								</div>
								
							</div>
							<div class="card-body">
								
								<p class="category conferences">
								<?php foreach ($category_title as $category):?>
									<?php echo $category->name . " "; ?>
								<?php endforeach; ?>
								</p>
								<h5 class="card-title"><?php echo $title ?></h5>
								
							</div>
							
						</div>
				 	</a>
				 
			<?php endwhile; ?>
			</div>
		<?php else: ?>
			
		<?php endif; ?>
		<?php wp_reset_query(); ?>
	



	

</div>


