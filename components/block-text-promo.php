
<?php global $promo; ?>
<?php 
		
		$primary_section = "";
		$secondary_section = "";
		if($promo['primary_button_section']){
			$primary_section = "#" . $promo['primary_button_section'];
		}
		if($promo['secondary_button_section']){
			$secondary_section = "#" . $promo['secondary_button_section'];
		}

 ?>
<div class="row">
	<div class="col text-center">
		<h1><?php echo $promo['header'] ?></h1>
		<p><?php echo $promo['content'] ?></p>
		<div class="button-row pt-1">
			<a href="<?php echo $promo['secondary_button_page'] . $secondary_section ?>"  class="btn btn-secondary mb-1 mb-sm-0"><?php echo $promo['secondary_button_text'] ?></a>
			<a href="<?php echo $promo['primary_button_page'] . $primary_section ?>"  class="btn btn-primary"><?php echo $promo['primary_button_text'] ?></a>
			
		</div>
	</div>
</div>