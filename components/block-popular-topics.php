		
<div class="col-lg-4 my-2">
	<div class="card" style="height:100%">
		  <div class="card-body padded-card">
		    <div class="talk-meta ">
		    	<span class="session">
		    		<?php 
		    			$sessions = get_field('session_length');
		    			if(1 == $sessions){
		    				$session_length = "Single Session";
		    			} else {
		    				$session_length = "$sessions Sessions";
		    			}

		    			echo $session_length;
		    		 ?>
		    	</span>
		    </div>
		    <h5 class="card-title"><?php the_title(); ?></h5>

		    <?php the_field('short_description'); ?>

		    <div class="modal fade topics-modal" id="topic-<?php the_ID() ?>" tabindex="-1" role="dialog" aria-labelledby="moreDetails" aria-hidden="true">
		    	<div class="modal-dialog" role="document">
		    	    <div class="modal-content">
		    			<div class="modal-header">
	    			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	    			          <span aria-hidden="true">&times;</span>
	    			        </button>
	    			    </div>
	    			    <div class="modal-body">
	    			    	<?php the_field('long_description') ?>
	    			    </div>
	    			    <div class="modal-footer">
    			           <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    			        </div>
		    		</div>
		    	</div>
		    </div>

		    
		    
		  </div>
		  <div class="card-footer">
		    <button class="btn btn-primary" data-toggle="modal" data-target="#topic-<?php the_ID() ?>">Learn More</button>
		  </div>
	</div>
</div>
		
		
	
	