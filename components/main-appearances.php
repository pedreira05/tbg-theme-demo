<div class="partners-content bg-dark-shade invert-text-color">
	<div class="container">
		<div class="row">

			
			<div class="col-md align-self-center py-3">
				<div class="col-sm-12">
					<div class="sylane-intro clearfix box-shadow">
						<div class="sylane-intro-img">
							<img src="<?php echo get_stylesheet_directory_uri() ?>/images/sylane_mack.jpg">
						</div>
						<div class="sylane-intro-content align-middle">
							<h5>Sylane Mack</h5>
							<p>President, Principle Speaker, Counselor</p>
						</div>
					</div>
				</div>
				<div class="appearing-on">
					<div class="align-bottom text-center pt-3">
						<h2>Sylane has appeared on these radio programs</h2>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="appearances text-center py-md-5">
					<div class="logos">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logos/CBS_Radio_logo.png">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logos/america_in_the_morning_logo.jpg">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logos/westwood_one_logo.png	">
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logos/Clear-Channel-01.png">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logos/92_KQRS_logo.jpg">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logos/1200_woai_logo.png">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logos/ETM_Logo_Beet_RGB.png">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logos/jim_bohannon_lgo.jpg">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logos/kfbk_news_talk_radio_logo.jpg">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logos/wcco_radio_logo.jpg">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="partners-footer bg-light-shade">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 align-self-center affiliates-row py-2 py-md-0">
				<p class="m-0 emphasis secondary">Transformed by Grace, Inc. is partnered with Compassion International.</p>
				<div class="align-self-center compassion-international-logo">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logos/compassion_international_logo.png">
				</div>
			</div>
		</div>
	</div>
</div>