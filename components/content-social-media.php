<li><i class="far fa-envelope"></i> P.O. Box 976, Newtown, PA 18940 </li>
<?php if( function_exists( 'eeb_content' ) ): ?>
<li><i class="fas fa-at"></i> <?php echo do_shortcode( '[eeb_email email="info@transformedbygrace.org" display="info@transformedbygrace.org"]' ) ?> </li>
<?php else: ?>
<li><i class="fas fa-at"></i> <a href="mailto:info@transformedbygrace.org">info@transformedbygrace.org</a> </li>
<?php endif; ?>
<li><i class="fas fa-phone"></i> (215) 497-0882 </li>
<li><i class="fab fa-facebook-f"></i> <a href="https://www.facebook.com/transformedbygrace.org/" target="_blank">transformedbygrace</a></li>
<!-- <li><i class="fab fa-instagram"></i> @transformedbygrace</li> -->