<div class="row">
	<div class="col text-center">
		<h1>Counseling for individuals, couples and families</h1>
		<p>Our counselors offer sound, biblically-based christian counseling and descipling to help people meet the challenges of life.</p>
		<div class="button-row pt-1">
			<a href="/about-us/counseling-ministry" type="submit" class="btn btn-secondary mb-1 mb-sm-0">Learn More</a href="">
			<button type="submit" class="btn btn-primary">Book an appointment</button>
		</div>
	</div>
</div>