<?php 
	$images = get_field('banner_image');
	$image = $images['sizes']['large'];
	$button = get_field('banner_cta_text');

 ?>

<section>
	<div class="jumbotron jumbotron-fluid image-filter page-banner" style="background:url(<?php echo $image; ?>) center; height:500px; background-size:cover">
	  <div class="banner-content-wrap h-100">
		  <div class="container h-100">
		  	<div class="row align-items-center h-100">
		  		<div class="col-lg-6">
				    <h1><?php the_field('banner_title') ?></h1>
				    <p><?php the_field('banner_introduction') ?></p>
				    <?php if($button): ?>
				    	<?php
				    		$primary_section = "";
				    		if(get_field('banner_cta_section')){
				    			$primary_section = "#" . get_field('banner_cta_section');
				    		}
				    	 ?>
				    	<a href="<?php the_field('banner_cta_page'); echo $primary_section ?>" class="btn btn-<?php the_field('banner_cta_style') ?>"><?php the_field('banner_cta_text') ?></a>
					<?php endif; ?>
		  			
		  		</div>
		  	</div>
		  </div>
	  </div>
	</div>
</section>