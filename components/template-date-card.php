<?php 
/**
 * $image
 * $date
 * $end_date
 * -- $start_date_month
 * -- $end_date_month
 * -- $start_day
 * -- $end_day
 * $category_title
 * $title
 * $article_id
 */
global $content_array;
$image = $content_array['image'];
$start_date = $content_array['start_date'];
$start_date_month = date("M", strtotime($start_date));
$start_day = date("d", strtotime($start_date));
$end_date = $content_array['end_date'];
$end_date_month = date("M", strtotime($end_date));
$category_title = $content_array['category_title'];

$title = $content_array['title'];
$post_id = $content_array['post_id'];
 ?>


<div class="card date-card box-shadow h-100">

	<div class="card-header" style="background: url(<?php echo $image ?>); background-size:cover">
		<div class="date">
			<span class="month"><?php echo $start_date_month ?></span> <br> 
			<span class="day"><?php echo $start_day; ?></span>
		</div>
		
	</div>
	<div class="card-body">
		
		<p class="category conferences">
		<?php foreach ($category_title as $category):?>
			<?php echo $category->name . " "; ?>
		<?php endforeach; ?>
		</p>
		<h5 class="card-title"><?php echo $title ?></h5>
		
	</div>
	
</div>