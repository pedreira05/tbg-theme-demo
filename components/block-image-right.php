<?php 
/* content_array variables defined on the page template before
* the template_part include is called
* */
global $content_array;

if( !empty($content_array['section']) && $content_array['section']=='events'){
	$alignment = "";
} else {
	$alignment = "align-self-center";
}
?>
<div class="container">
	<div class="row">
		<div class="col-sm <?php echo $alignment; ?>">
				<h2><?php echo $content_array['heading'] ?></h2>
				<?php echo $content_array['content'] ?>
				
		</div>
		<div class="col-sm <?php echo $alignment; ?>" style="background-size:cover">
			<img src="<?php echo $content_array['image'] ?>">
		</div>
	</div>
</div>