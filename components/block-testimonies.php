<div class="carousel-item <?php echo $active_class; ?>">
  <div class="testimony align-self-center">

    <div class="entry-content">
      <?php the_content() ?>
      <strong><?php the_field('authors_name') ?></strong>  
      <?php the_field('authors_organization') ?>
    </div>

  </div>
</div>