<?php global $staff ?>
<?php 
$first_name = $staff['first_name'];
$last_name = $staff['last_name'];
$titles = $staff['titles'];
 ?>
<div class="col-lg-4 col-md-6 mb-3">
	<a class="no-underline bio-link box-shadow rounded" style="display:block" href="#" data-toggle="modal" data-target="#staff-<?php echo $staff['ID'] ?>">
	<div class="card img-card h-100">
		
			<div class="staff-img" style="background:url(<?php echo $staff['image'] ?>) no-repeat center top; background-size:cover; height: 400px">
			</div>
		  <div class="card-body p-2">
		    <p class="card-title"><?php echo "$first_name $last_name, $titles" ?></p>
		  </div>
		 
	</div>
	</a>
    <div class="modal fade staffs-modal" id="staff-<?php echo $staff['ID'] ?>" tabindex="-1" role="dialog" aria-labelledby="staffDetails" aria-hidden="true">
    	<div class="modal-dialog" role="document">
    	    <div class="modal-content">
    			<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			    </div>
			    <div class="modal-body">
			    	<?php echo $staff['biography'] ?>
			    </div>
			    <div class="modal-footer">
		           <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
		        </div>
    		</div>
    	</div>
    </div>
</div>
