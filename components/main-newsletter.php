<?php 
$args = array(
    'posts_per_page' => 1,
    'category_name' => 'current-newsletters'
);
$newsletters = new WP_Query( $args);

 ?>



<div class="container">
	<div class="row">
		<div class="col-sm newsletters align-self-center">
		<?php if ( $newsletters->have_posts() ) : ?>
	   		<?php while ( $newsletters->have_posts() ) : ?>
	    	<?php $newsletters->the_post(); ?>
	    	<?php $post_id  = get_the_ID(); ?>
				<article id="<?php echo $post_id; ?>" class="post">
					<header class="entry-header">
						<h1 class="entry-section-head line-title">Latest Newsletter</h1>
						<h1 class="entry-title"><a href="<?php the_permalink( ) ?>"><?php the_title( ) ?></a></h1>
					</header><!-- .entry-header --> 

					<div class="entry-content">
						<?php the_excerpt(); ?>
					</div><!-- .entry-content -->
				</article>
		    <?php endwhile; ?>
		    <?php wp_reset_postdata(); ?>
		    <?php wp_reset_query(); ?>
			<?php endif; ?>
		</div>
		<div class="col-sm align-self-center">
			<div class="newsletter-signup-form bg-primary-accent invert-text-color p-4 rounded">

				<p>Subscribe to have our newsletter emailed to you</p>
				<?php echo do_shortcode( '[tbg_newsletter_registration_form]') ?>
				
			</div>
		</div>
		
	</div>
</div>