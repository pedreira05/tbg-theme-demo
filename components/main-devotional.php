<?php 
	/* gets all data for the custom field
	 group for this section */
	$gm = get_field('grace_moments');


	// get a devotional that has today's date
	// if a devotional for today's date doesn't exist,
	// then get the a devotional that has a previous date to today
	// e.g. if there is no Feb 29 post, then use Feb 28
	$day = date('d');
	$month = date('m');
	$args = array(
	    'posts_per_page' => 1,
	    'category_name' => 'devotions', 
	    'day'	=> $day,
	    'monthnum'	=> $month
	);
	$devotion = new WP_Query( $args);

	// Check if we have found a post. If we haven't, cycle backwards
	// in date until we find one
	if ( $devotion->found_posts < 1 ){
		do {
			$day--;
			$args['day'] = $day;
			$devotion = new WP_Query( $args);
		} while ( $devotion->found_posts < 1 );
	}

	
			

	

	 
?>

<div class="container">
	<div class="row">
		<div class="col-sm devotional align-self-center">
			<?php if ( $devotion->have_posts() ) : ?>
		    <?php while ( $devotion->have_posts() ) : ?>
		    <?php $devotion->the_post(); ?>
		    <?php $post_id  = get_the_ID(); ?>
			<article id="<?php echo $post_id ?>" class="post">
				<header class="entry-header">
					<h1 class="entry-section-head line-title"><?php echo $gm['section_title'] ?></h1>
					<h1 class="entry-title"><a href="<?php the_permalink( ) ?>"><?php the_title( ) ?></a></h1>
				</header><!-- .entry-header --> 

				<div class="entry-content">
					<?php the_excerpt(); ?>
				</div><!-- .entry-content -->
			</article>
		    <?php endwhile; ?>
		    <?php wp_reset_postdata(); ?>
		    <?php wp_reset_query(); ?>
			<?php endif; ?>
		</div>
		<div class="col-sm  align-self-center">
			<div class="grace-moments-promo">
				<div class="promo-card">
					<div class="promo-img">
				  		<img style="max-width:330px" class="rounded box-shadow" src="<?php echo get_stylesheet_directory_uri(); ?>/images/grace_moments_cover.jpg" alt="Card image cap">
						<div class="promo-content bg-dark-accent invert-text-color box-shadow rounded p-3">
							
						    <h5 class="card-title"><?php echo $gm['gm_caption'] ?></h5>
						    <p class="card-text"><?php echo $gm['gm_content']; ?></p>
						    <a href="<?php echo $gm['grace_moments_cta_page'] ?>" class="btn btn-<?php echo $gm['grace_moments_cta_style']; ?>"><?php echo $gm['grace_moments_cta_text'] ?></a>
						</div>
					</div>
				
				</div>
			</div>
			
		</div>
		
	</div>
</div>