<div class="container">
	<h1 class="component-header text-center">
		Partner with us
	</h1>
	<p class="text-center">Our Partners in Grace offer their gifts of prayer and financial support to enable us to faithfully carry out the purpose and passion of this ministry: to share biblical truths of the transforming grace of Jesus Christ with others!</p>
</div>


<div class="row ping-row no-gutters">		
	<div class="col-lg-4  text-center">
		<div class="contribute ping-secondary h-100 w-100" style="display:table">
		  <div class="h-100 card-body align-middle" style="display:table-cell">
		    <h5 class="card-title">Single Donation</h5>
		    <p class="card-text">Give a one-time offering</p>
		    <a href="#" class="btn btn-primary launch-single-donation" data-toggle="modal" data-target="#ping-form">Donate now</a>
		  </div>
		</div>
	</div>
	<div class="col-lg-4 text-center ">
		<div class="contribute ping-primary w-100" style="display:table">
		  <div class="h-100 card-body align-middle" style="display:table-cell">
		    <h5 class="card-title">Give Monthly</h5>
		    <p class="card-text">Give a fixed amount each month</p>
		    <a href="#" class="btn btn-primary launch-monthly-donation" data-toggle="modal" data-target="#ping-form">Donate now</a>
		</div>
    	    
		</div>
	</div>
	<div class="col-lg-4 text-center">
		<div class="contribute ping-secondary h-100 w-100" style="display:table">
		  <div class="h-100 card-body align-middle" style="display:table-cell">
		    <h5 class="card-title">Partner in Prayer</h5>
		    <p class="card-text">Subscribe to our newsletter to keep <br>
		    informed of how you can pray for us.</p>
		   	<?php echo do_shortcode( '[tbg_newsletter_registration_form]' ) ?>
		      <!-- <button type="submit" class="btn btn-primary">Subscribe</button> -->
		  </div>
		</div>
	</div>

</div>

<div class="modal fade ping-modal" id="ping-form" tabindex="-1" role="dialog" aria-labelledby="pingDetails" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
			<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		    </div>
		    <div class="modal-body">
		    	<?php echo do_shortcode( '[tbg_ping_form]' ); ?>
		    </div>
		</div>
	</div>
</div>
