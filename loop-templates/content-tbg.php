<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>
	<div class="p-4 archive-rows">
		
			<div class="col-md-10">
				
		
				<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
					
			
					<header class="entry-header">
						<?php if( in_category('devotions')): ?>
							<p class="accent-text"><?php echo get_the_date('F d'); ?></p>
						<?php endif; ?>
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			
					</header><!-- .entry-header -->
			
					
			
					<div class="entry-content">
			
						<?php the_excerpt(); ?>
			
						<?php
						wp_link_pages( array(
							'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
							'after'  => '</div>',
						) );
						?>
			
					</div><!-- .entry-content -->
			
					<footer class="entry-footer">
						
			
					</footer><!-- .entry-footer -->
			
				</article><!-- #post-## -->
			
		</div>
	</div>	
