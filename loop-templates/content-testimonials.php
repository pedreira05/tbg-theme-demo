<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>
	<div class="card archive-card mb-4 p-4 bg-desaturate-primary">
		<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
			
	
			<div class="entry-content">
	
				<?php the_content(); ?>
	
				<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
					'after'  => '</div>',
				) );
				?>
	
			</div><!-- .entry-content -->
	
			<footer class="entry-footer">
				<p class="accent-text"> 
					<?php the_field('authors_name') ?>
				</p>
				<p><?php the_field('authors_organization') ?></p>
	
			</footer><!-- .entry-footer -->
	
		</article><!-- #post-## -->
	</div>	
