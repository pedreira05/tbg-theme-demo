(function($) {
  
   
  $(".launch-single-donation").click(function(){
    $("input[name='donation_type']").val("one-time");
  });
  $(".launch-monthly-donation").click(function(){
    $("input[name='donation_type']").val("monthly");
  });

  $('.sendme').click(function(){

   var amount = $('#amount').val();
    var donationType = $("input[name='donation_type']").val();


   if (donationType == 'monthly'){

             //set the button type to the subcription button saved in the pay pal control panel for TBG account as noted by the number in the hosted_button_id field
             $('#buttontype').val("3MBXJF8TVMQGU");
             $('#item_name').val("PING subscription");
             $('#cmd').val("_xclick-subscriptions");
             $('#a3').val(amount);
             $('#test2').val(amount);


           } else if (donationType == 'one-time') {

            //set the button type to the donation button saved in the pay pal control panel for TBG account as noted by the number in the hosted_button_id field
            $('#buttontype').val("U96RYC8RCJ27L");
            $('#item_name').val("PING donation");
            $('#cmd').val("_donations");
            
          }
          if ($("input[name='prayerpartner']:checked").val() !== undefined) {
            var prayerPartner = "yes";
          } else {
            var prayerPartner = "no"
          }

            // place Ping commitment type into a string called text
            var texto = "\n" + "Donation Commitment: " + $(this).val() + "\n" + "Prayer Partner: " +  prayerPartner;

     // add the "text" string to the custom hidden field which will be sent via email to info@       
     $("#custom").val(texto);


   }); 

    // here $ would be point to jQuery object
    $(document).ready(function() {
        $(".single-donation-screen").find(".sendme").data("donationtype", "single");
        $(".montly-donation-screen").find(".sendme").data("donationtype", "montly");



    });
})(jQuery);