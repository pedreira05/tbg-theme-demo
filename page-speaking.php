<?php
/**
 * Template Name: Book a speaker Page
 *
 * 
 */

get_header();



?>

<?php get_template_part( 'components/block', 'banner' ) ?>

<section>
	<div class="container">
		
		<div class="row justify-content-center">
			<div class="col-lg-6">
				<div class="video-img-container banner-overlap">
					<?php the_field('featured_content') ?>
				</div>
			</div>	
		</div>
	</div>
</section>

<section class="announcement py-5">
	<?php 
		$heading = get_field('intro_and_image_heading');
		$content = get_field('intro_and_image_content');
		$image = get_field('intro_and_image_image');
		$content_array = array(
			'heading'		=>		$heading,
			'content'		=>		$content,
			'image'			=>		$image['sizes']['two_column_block']
		);
	 ?>
	<?php get_template_part( 'components/block', 'image-right' ) ?>
</section>

<section class="py-5 bg-dark-shade">
	<div class="container">
		<h1 class="text-center mb-3" style="color:#fff">Popular Topics</h1>
		<div class="row popular-topics-list">
			
	
			<?php
				$query_args = array(
					'post_type' => 'speaking_topic', 
					'posts_per_page' => 10,
					'tax_query' => array(
							array(
								'taxonomy' => 'tbg_speaking_topics',
								'field'    => 'slug',
								'terms'    => 'popular',
							),
						)
				);
				$loop = new WP_Query( $query_args ); 
			?>
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<?php get_template_part( 'components/block', 'popular-topics' ) ?>
			<?php endwhile; ?>
		</div>
		<div class="row">
			<div class="col text-center py-4">
				<button class="btn btn-secondary load-topics" data-toggle="collapse" data-target="#collapseExample">More Topics</button>
			</div>
		</div>
	</div>
</section>

<section class="book-sylane py-5">
	<div class="container">
			<div class="row">
				<div class="col text-center">
					<h1>Customized topics for your event</h1>
					<p>We are happy to develop other presentations to meet your organization’s specific focus and theme.  Contact us with your vision and our team will work with you to develop a program to match.</p>
					
					
				</div>
			</div>
	</div>
</section>



<section class="testimonies py-5 bg-dark-shade invert-text-color">
	<div class="container">
		<div class="header">
  			<h1 class="text-center">Testimonials</h1>
		</div>

		<div id="testimonies_slider" class="carousel slide" data-ride="false" data-interval="false" style=" margin: 0 auto">
			<div class="carousel-inner">
					<?php
						$query_args = array(
							'post_type' => 'post', 
							'category_name' => 'testimonials', 
							'posts_per_page' => 10
						);
						$loop = new WP_Query( $query_args );
						$slide_total = $loop->found_posts; 
						$slide_count = 0; 
					?>
					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
						<?php $active_class = ($slide_count == 0) ? 'active' : ''; ?>
						<div class="carousel-item px-4 <?php echo $active_class; ?>">
						  <div class="testimony align-self-center">
						    
						      <div class="entry-content">
						        <?php the_content() ?>
						        <p><strong><?php the_field('authors_name') ?></strong><br>   
						        <?php the_field('authors_organization') ?></p>
						      </div>
						   
						  </div>
						</div>
						<?php $slide_count++; ?>
					<?php endwhile; ?>
					<?php wp_reset_query() ?>

			  
			</div>
			<ol class="carousel-indicators px-4" style="bottom: -10px">
		    	<?php for ($i=0; $i < $slide_total; $i++) : ?>
		    	<?php $active_class = ($i == 0) ? 'active' : ''; ?>
		       <li data-target="#testimonies_slider" data-slide-to="<?php echo $i ?>"  class="<?php echo $active_class; ?>"></li>
		    	<?php endfor; ?>
		       
		     </ol>

		     
		     	
			  <a class="carousel-control-prev" href="#testimonies_slider" role="button" data-slide="prev">
			    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#testimonies_slider" role="button" data-slide="next">
			    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
		  
				
		</div>
	</div>
</section>



<section class="book-appointment py-5" id="book-event" name="book-event">
	<div class="container">
			<div class="row justify-content-center">
				
			
				<div class="col-lg-8">
					<div class="card bg-primary-accent invert-text-color p-4">
						
					
						<h2>Book Sylane for your Event</h2>
						<?php echo do_shortcode( '[tbg_contact_form]' ) ?>
					</div>
				</div>
			</div>
	</div>
</section>


<?php get_footer(); ?>
