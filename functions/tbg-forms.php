<?php 

/**
 * Custom TBG contact us form
 *
 * Enhanced by jquery validate. Download at https://jqueryvalidation.org/
 */

function tbg_contact_form_html(){ ?>
	<?php ob_start(); ?>
	<?php $form_status = "init" ?>
	<div id="tbg-contact-form">
		<?php if(isset($_GET['contact']) && $_GET['contact'] == true): ?>
			<?php $form_status = "sent"; ?>

			<div id="success_alert_message" class="alert-success">Thank you. Your message has been sent. </div>
			<script type="text/javascript">
				success_alert_message.scrollIntoView();
			</script>
		<?php endif; ?>
	    <form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" id="tbgContactForm" class="<?php echo $form_status ?>">
	    	

	    	<div class="form-group">
	    		<label for="fullname">Full Name<span class="required-indicator">*</span></label><br>
	    		<input type="text" name="fullname" id="fullname" required>
	    	</div>
	    	<div class="form-group">
	    		<label for="email">Email Address<span class="required-indicator">*</span></label><br>
	    		<input type="email" name="email" id="email" required>
	    	</div>
	    	<div class="form-group">
	    		<label for="message">Your Message</label><br>
	    		<textarea name="message" id="message" class="form-control"></textarea>
	    	</div>
	    	<input type="hidden" name="action" value="contact_form">
	    	<input type="hidden" name="page" value="<?php the_title(); ?>">
	    	<input type="hidden" name="return_to" value="<?php the_permalink(); ?>">
	    	<div class="form-group"><input type="submit" class="btn btn-primary" value="Send Message"></div>
	    	<?php wp_nonce_field('tbg-contact-form-nonce') ?>
	    </form>
	</div>
	<script type="text/javascript">
		$('#tbgContactForm').validate();
	</script>
	<?php $form = ob_get_clean(); ?>
	<?php return $form; ?>
<?php
}
add_shortcode( 'tbg_contact_form', 'tbg_contact_form_html' );


function tbg_ping_form_html(){ ?>
	<?php ob_start(); ?>
	<?php $form_status = "init" ?>
	<div id="tbg-ping-form">
		<?php if(isset($_GET['contact']) && $_GET['contact'] == true): ?>
			<?php $form_status = "sent"; ?>

			<div id="success_alert_message" class="alert-success">Thank you. Your donation has been sent. </div>
			<script type="text/javascript">
				success_alert_message.scrollIntoView();
			</script>
		<?php endif; ?>
	    

	    <form id="payPalForm" action="https://www.paypal.com/cgi-bin/webscr" method="post">
		    	<input id="donation_type" name="donation_type" type="hidden" value="monthly" />
		    	<input id="item_name" name="item_name" type="hidden" />
		    	<input id="cmd" name="cmd" type="hidden" /> 
		    	<input name="no_note" type="hidden" value="1" /> 
		    	<input name="business" type="hidden" value="info@transformedbygrace.org" /> 
		    	<input name="currency_code" type="hidden" value="USD" /> 
		    	<input name="return" type="hidden" value="<?php echo get_bloginfo( 'url' ) ?>" /> 
		    	<input id="hosted_button_id" name="hosted_button_id" type="hidden" /> 
		    	<input id="custom" name="custom" type="hidden" /> 
		    	<input id="a3" name="a3" type="hidden" /> 
		    	<input id="p3" name="p3" type="hidden" value="1" /> 
		    	<input id="t3" name="t3" type="hidden" value="M" /> 
		    	<input id="src" name="src" type="hidden" value="1" /> 
		    	<input id="srt" name="srt" type="hidden" value="12" /> 

		    	<div class="form-group">
			    	<label for="first_name">First Name<span class="required-indicator">*</span></label><br />
			    	<input id="first_name" class="form-control validates-as-required" name="first_name" required="" type="text" value="" aria-required="true" aria-invalid="false" />
		    	</div>
		    	<div class="form-group">
				    <label for="last_name">Last Name<span class="required-indicator">*</span></label><br />
				    <input id="last_name" class="form-control validates-as-required" name="last_name" required="" type="text" value="" />
		    	</div>

	    		<div class="form-group">
		    		<label for="field_1">Name of Organization</label><br />
		    	    <input id="field_1" class="text form-control" name="field_1" type="text" value="" />
	    		</div>

	    		

	  			<div class="form-group">
			    	<label for="amount">Amount<span class="required-indicator">*</span></label><br />
			    	<input id="amount" class="text" name="amount" required="" size="40" step="0.01" type="number" value="" />
	  			</div>

	    		<div class="form-group">
	    			<input id="sendme" class="btn btn-primary sendme" type="submit" value="Submit" data-donationtype="" /><img class="ajax-loader" style="visibility: hidden;" src="https://transformedbygrace.local/wp-content/plugins/contact-form-7/images/ajax-loader.gif" alt="Sending ..." />
	    		</div>


	    </form>
	    <script type="text/javascript">
	    	$('#payPalForm').validate();
	    </script>
	</div>
	<?php $form = ob_get_clean(); ?>
	<?php return $form; ?>
<?php
}
add_shortcode( 'tbg_ping_form', 'tbg_ping_form_html' );

function tbg_constant_contact_registration_form(){ ?>
	<?php ob_start(); ?>
	<?php $form_status = "init" ?>
	<div id="tbg-constant-contact-form">
		<?php if(isset($_GET['contact']) && $_GET['contact'] == true): ?>
			<?php $form_status = "sent"; ?>

			<div id="success_alert_message" class="alert-success">Thank you. Your donation has been sent. </div>
			<script type="text/javascript">
				success_alert_message.scrollIntoView();
			</script>
		<?php endif; ?>
	    <form id="constantContactForm" style="margin-bottom: 2;" action="https://visitor.r20.constantcontact.com/d.jsp" method="post" name="ccoptin" target="_blank"><input name="llr" type="hidden" value="486gp7iab" /> <input name="m" type="hidden" value="1109057042303" /> <input name="p" type="hidden" value="oi" />
	    	<div  class="form-group" style="margin-bottom:0.625em">
		    	<label for="ea" class="assistive-text">Email:</label>
		    	<input name="ea" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required="" type="email" value="" placeholder="Enter Email" />
	    	</div>
	    	<div class="form-group" >
	    		<input class="btn btn-primary"  name="go" type="submit" value="Subscribe" />
	    	</div>

	    </form>
	    <script type="text/javascript">
	    	$('#constantContactForm').validate();
	    </script>
	<?php $form = ob_get_clean(); ?>
	<?php return $form; ?>
<?php
}
add_shortcode( 'tbg_newsletter_registration_form', 'tbg_constant_contact_registration_form' );


function prefix_send_email_to_admin() {
    /**
     * At this point, $_GET/$_POST variable are available
     *
     * We can do our normal processing here
     */ 

    // Sanitize the POST field
    $nonce = $_POST['_wpnonce'];   
        if ( ! wp_verify_nonce( $nonce, 'tbg-contact-form-nonce' ) )
            die ( 'Busted!');

    //setup variables
    $name = $_POST['fullname'];
    $email = $_POST['email'];

    // verify content
    if(!is_email( $email ))
    	die('Invalid email');


    // Generate email content
    $env = get_tld();

    if($env == "local"){
    	$to = 'curtis005@gmail.com';
    } else {
    	$to = 'info@transformedbygrace.org';
    }


    //compose message
   
    
    $body = "<p><strong>$name</strong> <br />";
    $body .= "$email</p>";

    $page = $_POST['page'];
    $subject = 'New message from the '. $page . ' page';
    $body .= nl2br($_POST['message']);
    $headers = array('Content-Type: text/html; charset=UTF-8');
    $return_to = $_POST['return_to'];


   


    // Send to appropriate email
    wp_mail( $to, $subject, $body, $headers );

    wp_redirect( $return_to . '?contact=true');


}
add_action( 'admin_post_nopriv_contact_form', 'prefix_send_email_to_admin' );
add_action( 'admin_post_contact_form', 'prefix_send_email_to_admin' );



function get_tld(){
	$url =   $_SERVER['SERVER_NAME']; 
	$domain = explode(".", $url); 
	return end($domain);
}


 ?>