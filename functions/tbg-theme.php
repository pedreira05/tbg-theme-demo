<?php 

// remove filters from understrap theme that conflict with the custom work we're doing
function remove_understrap_parent_filters() {
    remove_filter( 'wp_trim_excerpt', 'understrap_all_excerpts_get_more_link');
}
add_action( 'init', 'remove_understrap_parent_filters' );


// image sizes
add_image_size( $name='main_slider', $width = 1000, $height = 600, $crop = true );
add_image_size( $name='page_banner', $width = 1800, $height = 400, $crop = true );
add_image_size( $name='card_head', $width = 350, $height = 200, $crop = true );
add_image_size( $name='two_column_block', $width = 600, $height = 500, $crop = true );
add_image_size( $name = 'square_medium', $width = 400, $height = 400, $crop = true );

// get link to a category archive
function tbg_get_category_link($category_slug){
	$idObj = get_category_by_slug($category_slug); 
	$id = $idObj->term_id;
	$category_link = get_category_link( $id );
	return $category_link;
}


// custom after excerpt
if ( ! function_exists( 'tbg_excerpt_more_link' ) ) {
	/**
	 * Adds a custom read more link to all excerpts, manually or automatically generated
	 *
	 * @param string $post_excerpt Posts's excerpt.
	 *
	 * @return string
	 */
	function tbg_excerpt_more_link( $post_excerpt ) {

		return $post_excerpt . ' ...<p><a class="understrap-read-more-link" href="' . esc_url( get_permalink( get_the_ID() )) . '">' . __( 'Continue Reading &rarr;',
		'understrap' ) . '</a></p>';
	}
}
add_filter( 'wp_trim_excerpt', 'tbg_excerpt_more_link' );


//register menus
register_nav_menus( array(  
  'footer_left' => __( 'Footer Left Menu', 'tbg' ),  
  'footer_right' => __('Footer Right Menu', 'tbg')  
) );

/**
 * Sorts events on the archive page acording to the value in 
 * the date field
 * 	
 * 
 */
function sort_event_archives( $query ) {
    if ( is_post_type_archive('cpt_event') ) {
        $query->set('orderby','meta_value');
        $query->set('order','DESC');
        $query->set('meta_key','date');
        $query->set('meta_type','DATETIME');
    }
}
add_action( 'pre_get_posts', 'sort_event_archives' );

/**
 * Enabel Custom Style menu for tiny MCE
 */
function wpb_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
// add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

/*
* Callback function to filter the MCE settings
*/
 
function my_mce_before_init_insert_formats( $init_array ) {  
 
// Define the style_formats array
 
    $style_formats = array(  
/*
* Each array child is a format with it's own settings
* Notice that each array has title, block, classes, and wrapper arguments
* Title is the label which will be visible in Formats menu
* Block defines whether it is a span, div, selector, or inline style
* Classes allows you to define CSS classes
* Wrapper whether or not to add a new block-level element around any selected elements
*/
        array(  
            'title' => 'Subtitle',  
            'block' => 'div',  
            'classes' => 'subtitle',
            'wrapper' => true,
             
        ),  
        
    );  
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );  
     
    return $init_array;  
   
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' ); 



 ?>