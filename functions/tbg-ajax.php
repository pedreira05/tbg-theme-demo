<?php 


/**
 * Test center
 */

add_action( 'wp_enqueue_scripts', 'tbg_load_topics_scripts' );
add_action( 'wp_ajax_ajax-loadTopics', 'tbg_load_topics' );
add_action( 'wp_ajax_nopriv_ajax-loadTopics', 'tbg_load_topics' );
function tbg_load_topics_scripts() {
  wp_enqueue_script( 'inputtitle_submit', get_template_directory_uri() . '/js/inputtitle_submit.js', array( 'jquery' ) );
  wp_localize_script( 'inputtitle_submit', 'PT_Ajax', array(
      'ajaxurl'   => admin_url( 'admin-ajax.php' ),
      'nextNonce' => wp_create_nonce( 'myajax-next-nonce' )
    )
  );
}
function tbg_load_topics() {
  // check nonce
  $nonce = $_POST['nextNonce'];
  if ( ! wp_verify_nonce( $nonce, 'myajax-next-nonce' ) ) {
    die ( 'Busted!' );
  }

  //get speaking topics
  $query_args = array(
    'post_type' => 'speaking_topic', 
    'posts_per_page' => 10,
    'tax_query' => array(
        array(
          'taxonomy' => 'tbg_speaking_topics',
          'field'    => 'slug',
          'terms'    => 'popular',
          'operator' => 'NOT IN',
        ),
      )
  );
  $loop = new WP_Query( $query_args ); 

  ob_start();
  ?>
      
      <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <?php get_template_part( 'components/block', 'popular-topics' ) ?>
      <?php endwhile; ?>
<?
  wp_reset_postdata();
 
  $topics = ob_get_contents();
  ob_end_clean();

  // generate the response
  $response = json_encode( $topics );
  // response output
  header( "Content-Type: application/json" );
  echo $response;
  // IMPORTANT: don't forget to "exit"
  exit;
}





add_action('wp_footer', 'load_ajax_scripts');
function load_ajax_scripts() { ?>
  <script>

    (function ($) {
      function loadTopics(){
          
          $.ajax({
              type: 'POST',
              dataType: 'JSON',
              url: PT_Ajax.ajaxurl,
              data:{
                  action:'ajax-loadTopics',
                  nextNonce : PT_Ajax.nextNonce
              },
              success: function(filters){
                  // upon success, remove current data in the table
                  // and add the filtered results
                  $(".popular-topics-list").append(filters);
                  $('.load-topics').hide();

                      
              },
              error: function(){
                  console.log("no go");
              }
          });

       
        return false;
      }


      $(document).ready(function () {
        
        

        $('.load-topics').click(function () {
          loadTopics();
          return false;
        });

      });
    })(jQuery);
  </script>
  <?php
} 



 ?>