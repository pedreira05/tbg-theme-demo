<?php


get_header();
?>

<section class="mb-4">
	<div class="jumbotron jumbotron-fluid image-filter page-banner" style="background: url(<?php echo get_stylesheet_directory_uri() . '/images/bg-convinced.jpg' ?>); background-size: cover; height:250px;">
	  <div class="banner-content-wrap">
		  <div class="container">
		  	<div class="row align-items-center">
		  		<div class="col-6">
				   
		
		  		</div>
		  	</div>
		  </div>
	  </div>
	</div>
</section>
<section>
	<div class="container">
		<div class="row">
			<div class="col pb-4 text-center">
				
				<h1 style="font-size: 80px;" class="mb-0">404</h1>
				<p>Oops...Page not found</p>

				<a href="<?php echo get_bloginfo( 'url' ) ?>" class="btn btn-primary">Go to homepage</a>
			</div>
			
		
		</div>
	</div>
</section>

<?php get_footer(); ?>
