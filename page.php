<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
?>
<section class="mb-4">
	<div class="jumbotron jumbotron-fluid image-filter page-banner">
	  <div class="banner-content-wrap">
		  <div class="container">
		  	<div class="row align-items-center">
		  		<div class="col-6">
				   <?php the_title( $before = '<h1>', $after = '</h1>', $echo = true ) ?>
		
		  		</div>
		  	</div>
		  </div>
	  </div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-8 pb-4">
				
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'loop-templates/content', 'page' ); ?>
					

				<?php endwhile; // end of the loop. ?>
			</div>
			<div class="col">
				

			</div>
		
		</div>
	</div>
</section>

<?php get_footer(); ?>
