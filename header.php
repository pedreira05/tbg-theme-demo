<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<meta name="og:image" content="<?php echo get_stylesheet_directory_uri() ?>/images/grace_moments_3d_cover.jpg"/>
	<meta name="og:image" content="<?php echo get_stylesheet_directory_uri() ?>/images/cover-snapshot.jpg"/>
	<meta name="og:url" content="<?php echo get_bloginfo( 'url' ) ?>"/>
	<meta name="og:title" content="Transformed by Grace"/>
	<meta name="og:type" content="website" />
	<meta name="og:description" content="Conference Speaking and Counseling – For the glory of God and the healing of hearts and transforming of lives! &quot;For I am convinced that…nothing in all creation will be able to separate us from the love of God that is in Christ Jesus our Lord."/>
 	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/favicon.ico">
	<meta name="msapplication-TileColor" content="#00a300">
	<meta name="msapplication-config" content="/images/icons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="hfeed site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" class="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

		<div class="nav-container">
			
			

			<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

					<div class="secondary-nav-wrap">
						<div class="row">
							<div class="col-sm-12">
								<nav class="navbar navbar-expand-md navbar-light secondary-nav">
									<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
										<span class="navbar-toggler-icon"></span>
									</button>

									<!-- The WordPress Menu goes here -->
									<?php wp_nav_menu(
										array(
											'theme_location'  => 'primary',
											'container_class' => 'collapse navbar-collapse',
											'container_id'    => 'navbarNavDropdown',
											'menu_class'      => 'navbar-nav ml-auto',
											'fallback_cb'     => '',
											'menu_id'         => 'main-menu',
											'depth'           => 2,
											'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
										)
									); ?>
								</nav><!-- .site-navigation -->
							</div>
						</div>
					</div>

				<div class="c">
					<div class="marketing-nav navbar">
							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><img src="<?php echo get_stylesheet_directory_uri() . "/images/demo/tbg_logo.jpg" ?>" style="width:160px;"></a>
							<ul class="nav">
							  <li class="nav-item">
							    <a class="nav-link active" href="speaking-topics">Invite Sylane Mack to Speak</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" href="about-us/counseling-ministry">Book a Counseling Appointment</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" href="order-books">Order Books</a>
							  </li>
							  <li class="nav-item">
							    <a class="btn btn-primary" href="partners-in-grace">Partner with Us</a>
							  </li>
							</ul>
					</div>
				</div>
			</div>

	</div><!-- #wrapper-navbar end -->
